console.log('Hello World');

let firstName = 'Alje', lastName = 'Earl', age = 34,
hobbies = ['Playing Online Games', 'Mountain Biking', 'Playing Drums/Guitar'],
workAddress = {
	houseNumber: "32", street: "Tres de Abril", city: "Cebu", barangay: "Punta Princesa"
};
console.log(`First Name: ${firstName}`);
console.log(`Last Name: ${lastName}`);
console.log(`Age: ${age}`);
console.log('Hobbies:');
console.log(hobbies);
console.log('Work Address:');
console.log(workAddress);

//--------------------------------------------

function printUserInfo() {
	console.log(`${firstName} ${lastName} is ${age} years of age`);
	console.log("This was printed inside of the function");
	console.log(hobbies);
	console.log("This was printed inside of the function");
	console.log(workAddress);
}

printUserInfo();

//--------------------------------------------

function returnFunction(isMarried) {
	return isMarried
};
let status = returnFunction(true)
console.log(`The value of isMarried is: ${status}`);

